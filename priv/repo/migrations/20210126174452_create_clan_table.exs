defmodule ClanServer.Repo.Migrations.CreateClanTable do
  use Ecto.Migration

  def change do
    create table(:clans) do
      add :name, :string
      add :tag, :string
      add :main_id, references(:users, on_delete: :nothing)
    end
    
    create index(:clans, [:main_id])
    create unique_index(:clans, [:tag, :name])
  end
end
