defmodule ClanServer.Repo.Migrations.CreateUsersTable do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :nickname, :string
    end

    create unique_index(:users, [:nickname])
  end
end
