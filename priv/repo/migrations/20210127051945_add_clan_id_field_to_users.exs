defmodule ClanServer.Repo.Migrations.AddClanIdFieldToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :clan_id, references(:clans, on_delete: :nothing)
    end

    create index(:users, [:clan_id])
  end
end
