defmodule ClanServer.RequestHandler do
  @moduledoc """
    Тут обрабатываются запросы из консоли
  """

  alias ClanServer
  alias ClanServer.{Users, Clans}

  def create_clan(clan_name, clan_tag, user_id) do
    user = Users.get_user!(user_id)

    ClanServer.create_clan(clan_name, clan_tag, user)
  end

  def invite_player(player_id, clan_id) do
    player = Users.get_user!(player_id)
    clan = Clans.get_clan!(clan_id)

    ClanServer.invite_player(player, clan)
  end

  def get_players(main_user_id, clan_id) do
    main_user = Users.get_user!(main_user_id)
    clan = Clans.get_clan!(clan_id)

    ClanServer.get_players(main_user, clan)
  end

  def invitation_request(player_id, clan_id) do
    player = Users.get_user!(player_id)
    clan = Clans.get_clan!(clan_id)

    ClanServer.invitation_request(player, clan)
  end

  def add_player(main_user_id, clan_id, player_id) do
    main_user = Users.get_user!(main_user_id)
    player = Users.get_user!(player_id)
    clan = Clans.get_clan!(clan_id)

    ClanServer.add_player(main_user, clan, player)
  end

  def accept_invitation(player_id, clan_id) do
    player = Users.get_user!(player_id)
    clan = Clans.get_clan!(clan_id)

    ClanServer.accept_invitation(player, clan)
  end

  def check_invitations(player_id) do
    player = Users.get_user!(player_id)

    ClanServer.check_invitations(player)
  end

  def decline_invitation(player_id, clan_id) do
    player = Users.get_user!(player_id)
    clan = Clans.get_clan!(clan_id)


    ClanServer.decline_invitation(player, clan)
  end

  def reject_request(main_user_id, clan_id, player_id) do
    main_user = Users.get_user!(main_user_id)
    player = Users.get_user!(player_id)
    clan = Clans.get_clan!(clan_id)

    ClanServer.reject_request(main_user, clan, player)
  end

  def remove_player(main_user_id, clan_id, player_id) do
    main_user = Users.get_user!(main_user_id)
    player = Users.get_user!(player_id)
    clan = Clans.get_clan!(clan_id)

    ClanServer.remove_player(main_user, clan, player)
  end
end