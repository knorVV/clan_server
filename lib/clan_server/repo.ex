defmodule ClanServer.Repo do
  use Ecto.Repo,
    otp_app: :clan_server,
    adapter: Ecto.Adapters.Postgres
end
