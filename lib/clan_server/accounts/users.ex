defmodule ClanServer.Users do
  @moduledoc """
    Users context.
  """
  import Ecto.Query, warn: false

  alias ClanServer.Users.User
  alias ClanServer.Repo

  @doc """
    Создание пользователя
  """
  @spec create_user(map()) :: {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
    Обновление пользователя
    Так же используется для изменения привязки к клану
  """
  @spec update_user(User.t(), map()) :: {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
    Получение пользователя
  """
  @spec get_user!(non_neg_integer()) :: User.t() | any()
  def get_user!(id) do
    Repo.get!(User, id)
  end
end