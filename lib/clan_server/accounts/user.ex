defmodule ClanServer.Users.User do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset

  alias __MODULE__
  alias ClanServer.Clans.Clan

  schema "users" do
    field :nickname, :string # Ник пользователя
    belongs_to :clan, Clan # Клан, в котором пользователь
  end

  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:nickname, :clan_id])
    |> validate_required([:nickname])
    |> validate_length(:nickname, min: 3, max: 12)
    |> unique_constraint(:nickname)
  end

end