defmodule ClanServer do
  @moduledoc """
    GenServer for clan_server
  """
  use GenServer

  alias ClanServer.Clans
  alias ClanServer.Clans.Clan
  alias ClanServer.Users.User

  def start_link(_) do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  def init(_) do
    {:ok, %{"invitations" => %{}, "requests" => %{}}}
  end

  def handle_call({:create_clan, clan_name, clan_tag, user}, _from, state) do
    reply = Clans.make_clan(clan_name, clan_tag, user)

    {:reply, reply, state}
  end

  def handle_call({:get_players, main_user, clan}, _from, state) do 
    reply =
      if Clans.is_clan_main_user?(clan, main_user) do
        requests = state["requests"]

        get_players_from_requests(clan, requests)
      else
        "The user is not the master of this clan"
      end

    {:reply, reply, state}
  end

  def handle_call({:add_player, main_user, clan, player}, _from, state) do
    {reply, state} =
      case Clans.add_player_by_main_user(clan, main_user, player) do
        :ok ->
          requests = state["requests"]
          updated_requests = remove_player_from_requests(player, clan, requests)
          state = Map.put(state, "requests", updated_requests)
          {:ok, state}

        error ->
          {error, state}
      end

    {:reply, reply, state}
  end

  def handle_call({:check_invitations, player}, _from, state) do
    invitations = state["invitations"]
    reply = get_clans_from_invitations(player, invitations)

    {:reply, reply, state}
  end

  def handle_call({:accept_invitation, clan, player}, _from, state) do
    {reply, state} =
      case Clans.add_player(clan, player) do
        :ok ->
          invitations = state["invitations"]
          updated_invitations = remove_clan_from_invitations(player, clan, invitations)
          state = Map.put(state, "invitations", updated_invitations)
          {:ok, state}

        error ->
          {error, state}
      end

    {:reply, reply, state}
  end

  def handle_call({:remove_player, main_user, clan, player}, _from, state) do
    reply =
      case Clans.remove_player(main_user, clan, player) do
        :ok ->
          :ok
        false ->
          "The user is not the master of this clan"
        error ->
          error
      end

    {:reply, reply, state}
  end

  def handle_cast({:invitation_request, player, clan}, state) do
    requests = state["requests"]

    updated_requests = put_request_to_state(player, clan, requests)
    state = Map.put(state, "requests", updated_requests)

    {:noreply, state}
  end
  
  def handle_cast({:invite_player, player, clan}, state) do
    invitations = state["invitations"]

    updated_invitations = put_invite_to_state(player, clan, invitations)
    state = Map.put(state, "invitations", updated_invitations)

    {:noreply, state}
  end

  def handle_cast({:decline_invitation, player, clan}, state) do
    invitations = state["invitations"]

    updated_invitations = remove_clan_from_invitations(player, clan, invitations)
    state = Map.put(state, "invitations", updated_invitations)

    {:noreply, state}
  end

  def handle_cast({:reject_request, main_user, clan, player}, state) do
    state =
      if Clans.is_clan_main_user?(clan, main_user) do
        requests = state["requests"]

        updated_requests = remove_player_from_requests(player, clan, requests)
        Map.put(state, "requests", updated_requests)
      else
        state
      end

    {:noreply, state}
  end

  def create_clan(clan_name, clan_tag, user) do
    GenServer.call(__MODULE__, {:create_clan, clan_name, clan_tag, user})
  end

  def invite_player(player, clan) do
    GenServer.cast(__MODULE__, {:invite_player, player, clan})
  end

  def invitation_request(player, clan) do
    GenServer.cast(__MODULE__, {:invitation_request, player, clan})
  end

  def get_players(main_user, clan) do
    GenServer.call(__MODULE__, {:get_players, main_user, clan})
  end

  def add_player(main_user, clan, player) do
    GenServer.call(__MODULE__, {:add_player, main_user, clan, player})
  end

  def accept_invitation(player, clan) do
    GenServer.call(__MODULE__, {:accept_invitation, clan, player})
  end

  def check_invitations(player) do
    GenServer.call(__MODULE__, {:check_invitations, player})
  end

  def decline_invitation(player, clan) do
    GenServer.cast(__MODULE__, {:decline_invitation, player, clan})
  end

  def reject_request(main_user, clan, player) do
    GenServer.cast(__MODULE__, {:reject_request, main_user, clan, player})
  end

  def remove_player(main_user, clan, player) do
    GenServer.call(__MODULE__, {:remove_player, main_user, clan, player})
  end

  # Добавляет игрока в state под ключом клана
  defp put_request_to_state(player, clan, requests) do
    %Clan{tag: clan_tag} = clan

    case Map.get(requests, clan_tag) do
      nil ->
        Map.put(requests, clan_tag, [player])

      players_list ->
        Map.put(requests, clan_tag, [player | players_list])
    end
  end

  # Добавляет слан в state под ключом ника игрока
  defp put_invite_to_state(player, clan, invitations) do
    %User{nickname: nickname} = player

    case Map.get(invitations, nickname) do
      nil ->
        Map.put(invitations, nickname, [clan])

      clans_list ->
        Map.put(invitations, nickname, [clan | clans_list])
    end
  end

  defp remove_player_from_requests(player, clan, requests) do
    players_list = get_players_from_requests(clan, requests)

    List.delete(players_list, player)
  end

  defp remove_clan_from_invitations(player, clan, invitations) do
    clans_list = get_clans_from_invitations(player, invitations)

    List.delete(clans_list, clan)
  end

  # Получаем всех игроков по ключу
  defp get_players_from_requests(clan, requests) do
    %Clan{tag: clan_tag} = clan

    Map.get(requests, clan_tag)
  end

  # Получаем все кланы по ключу
  defp get_clans_from_invitations(player, invitations) do
    %User{nickname: nickname} = player

    Map.get(invitations, nickname)
  end
end