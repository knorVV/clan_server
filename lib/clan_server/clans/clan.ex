defmodule ClanServer.Clans.Clan do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset

  alias __MODULE__
  alias ClanServer.Users.User

  schema "clans" do
    field :name, :string # Наименование клана
    field :tag, :string # Тег клана.
    belongs_to :main, User # Лидер клана
    has_many :players, User # Игроки клана
  end

  def changeset(%Clan{} = clan, attrs) do
    clan
    |> cast(attrs, [:name, :tag, :main_id])
    |> validate_required([:name, :tag, :main_id])
    |> validate_length(:tag, is: 3)
    |> unique_constraint(:name)
    |> unique_constraint(:tag)
  end
end