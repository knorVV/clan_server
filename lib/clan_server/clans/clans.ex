defmodule ClanServer.Clans do
  @moduledoc """
    Clans context.
  """
  import Ecto.Query, warn: false

  alias ClanServer.{
    Users,
    Repo
  }
  alias ClanServer.Users.User
  alias ClanServer.Clans.Clan

  @doc """
    Получение клана
  """
  @spec get_clan!(non_neg_integer()) :: Clan.t() | any()
  def get_clan!(id) do
    Repo.get!(Clan, id)
  end

  @doc """
    Формирование данных и создание клана
  """
  @spec make_clan(String.t(), String.t(), User.t()) ::
    {:ok, Clan.t()} | {:error, Ecto.Changeset.t()} | any()
  def make_clan(name, tag, user) do
    Repo.transaction(fn ->
      with clan_attrs = collect_clan_attrs(name, tag, user), # сборка необходимых параметров в map
          {:ok, %Clan{id: clan_id} = clan} <- create_clan(clan_attrs),
          {:ok, _user} <- Users.update_user(user, %{clan_id: clan_id})
      do
        clan
      end
    end)
  end

  @doc """
    Создание клана
  """
  @spec create_clan(map()) :: {:ok, Clan.t()} | {:error, Ecto.Changeset.t()}
  def create_clan(attrs \\ %{}) do # Можно сделать приватным
    %Clan{}
    |> Clan.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
    Добавление в клан пользователя лидером клана
    Заключается только в том, что в user обавляется/меняется зависимость от клана
  """
  @spec add_player_by_main_user(Clan.t(), User.t(), User.t()) :: :ok | {:error, any()} | false
  def add_player_by_main_user(%Clan{id: clan_id} = clan, main_user, player) do
    with true <- is_clan_main_user?(clan, main_user),
         {:ok, %User{}} <- Users.update_user(player, %{clan_id: clan_id})
    do
      :ok
    else
      false ->
        "The user is not the master of this clan"
      error ->
        error
    end
  end

  @doc """
    Добавление в клан пользователя автоматически
    Заключается только в том, что в user обавляется/меняется зависимость от клана
  """
  @spec add_player(Clan.t(), User.t()) :: :ok | {:error, any()}
  def add_player(%Clan{id: clan_id}, player) do
    case Users.update_user(player, %{clan_id: clan_id}) do
      {:ok, %User{}} ->
        :ok
      error ->
        error
    end
  end


  @doc """
    Удаление пользователя из клана
  """
  @spec remove_player(Clan.t(), User.t(), User.t()) :: :ok | {:error, any()} | false
  def remove_player(clan, main_user, player) do
    with true <- is_clan_main_user?(clan, main_user),
         {:ok, %User{}} <- Users.update_user(player, %{clan_id: nil})
    do
      :ok
    end
  end

  # Проверка лидера клана
  @spec is_clan_main_user?(Clan.t(), User.t()) :: true | false
  def is_clan_main_user?(clan, main_user) do
    %Clan{main_id: main_id} = clan
    %User{id: main_user_id} = main_user

    main_id == main_user_id
  end

  @spec collect_clan_attrs(String.t(), String.t(), User.t()) :: map() | any()
  defp collect_clan_attrs(name, tag, user) do
    %User{id: user_id} = user

    %{
      name: name,
      tag: tag,
      main_id: user_id
    }
  end
end