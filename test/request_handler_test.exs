defmodule ClanServer.RequestHandlerTest do
  use ClanServer.DataCase

  alias ClanServer.Users
  alias ClanServer.RequestHandler
  alias ClanServer.Clans.Clan

  @main_user_attrs %{nickname: "nicname"}
  # @player_attrs %{nickname: "player_nickname"}
  
  describe "Request to " do
    setup :create_user

    test "create_clan/3", %{main_user: main_user} do
      assert {:ok, %Clan{} = clan} = RequestHandler.create_clan("clan_name", "clan_tag", main_user.id)
      assert clan.name == "clan_name"
      assert clan.tag == "clan_tag"
      assert clan.main_id == main_user.id
    end
  end

  defp create_user(_) do
    {:ok, user} = Users.create_user(@main_user_attrs)

    {:ok, user: user}
  end

  defp create_clan(%{user: user}) do
    {:ok, clan} = RequestHandler.create_clan("clan_name", "clan_tag", user.id)

    {:ok, clan: clan}
  end
end