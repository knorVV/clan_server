import Config

config :clan_server, ClanServer.Repo,
  database: "clan_server_repo_test",
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :clan_server,
  ecto_repos: [ClanServer.Repo]