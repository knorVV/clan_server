import Config

config :clan_server, ClanServer.Repo,
  database: "clan_server_repo",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"

config :clan_server,
  ecto_repos: [ClanServer.Repo]

import_config "#{Mix.env()}.exs"